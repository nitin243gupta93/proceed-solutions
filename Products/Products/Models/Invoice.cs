﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Products.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public double? Tax { get; set; }
        public double? SubTotal { get; set; }
        public double? Total { get; set; }
        public ICollection<Product> Items { get; set; }
    }
}