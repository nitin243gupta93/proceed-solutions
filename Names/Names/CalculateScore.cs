﻿using System.Collections.Generic;
using System.Linq;

namespace Names
{
    public class CalculateScore : INames
    {
        public IEnumerable<string> GetNames(string path)
        {
            return System.IO.File.ReadAllText(@"" + path).Split(',').ToList();
        }

        public int CalculateTotal(IEnumerable<string> names)
        {
            var index = 0;
            var result = 0;
            var orderedList = names.OrderBy(word => word);

            foreach (var word in orderedList)
            {
                index++;
                result += index * GetAlphabeticalSum(word.Trim('"'));
            }

            return result;
        }

        public int GetAlphabeticalSum(string name)
        {
            var result = 0;

            foreach (var c in name)
                result = result + (char.ToUpper(c) - 64);

            return result;
        }
    }
}
