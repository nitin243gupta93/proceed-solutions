namespace Products.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {

            CreateTable(
                "dbo.Provinces",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    FederalTax = c.Double(),
                    ProvincialTax = c.Double(),
                })
                .PrimaryKey(t => t.Id);
            CreateTable(
                "dbo.Products",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    Price = c.Double(),
                    ProvinceId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Provinces", t => t.ProvinceId, cascadeDelete: true)
                .Index(t => t.ProvinceId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Products", "ProvinceId", "dbo.Provinces");
            DropIndex("dbo.Products", new[] { "ProvinceId" });
            DropTable("dbo.Provinces");
            DropTable("dbo.Products");
        }
    }
}
