﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            var current = 0;
            var previous = 1;
            var total = 0;
            var maximum = 4000000;
            var flag = true;
            while (flag)
            {
                var temporary = current;
                current = current + previous;
                previous = temporary;

                if (current % 2 == 0 && !(current >= maximum)) //Check for even values and no value mroe than 4000000 is added
                    total += current;
                if (current >= maximum)
                    flag = false;
            }
            Console.WriteLine(total);
            Console.ReadLine();
        }
    }
}
