﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Products.Models;
using Products.Services;

namespace Products.Controllers
{
    [RoutePrefix("Api/Products")]
    public class ProductsController : ApiController
    {
        private readonly IContext<Product> _context;

        public ProductsController()
        {
            _context = new ProductService();
        }
        public ProductsController(IContext<Product> context)
        {
            _context = context;
        }

        // GET: api/Products
        public IEnumerable<Product> GetProducts()
        {
            return _context.GetAll();
        }

        //GET Api/Products/AllProducts using Attribute Routing 
        [HttpGet, Route("AllProducts")]
        public IEnumerable<Product> GetAllProducts()
        {
            return _context.GetAll();
        }

        // GET: api/Products/5
        [HttpGet, Route("{id}")]
        public Product GetProduct(int id)
        {
            Product product = _context.Get(id);
            if (product == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return product;
        }
    }
}