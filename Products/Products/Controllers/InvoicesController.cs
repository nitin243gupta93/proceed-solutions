﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Products.Models;
using Products.Services;

namespace Products.Controllers
{
    [RoutePrefix("Api/Invoices")]
    public class InvoicesController : ApiController
    {
        private readonly IContext<Invoice> _context;

        public InvoicesController()
        {
            _context = new InvoiceService();
        }
        public InvoicesController(IContext<Invoice> context)
        {
            _context = context;
        }

        // GET: api/Invoices
        public IEnumerable<Invoice> GetInvoices()
        {
            return _context.GetAll();
        }

        //GET Api/Invoices/AllInvoices using Attribute Routing 
        [HttpGet, Route("AllInvoices")]
        public IEnumerable<Invoice> GetAllInvoices()
        {
            return _context.GetAll();
        }

        // GET: api/Invoices/5
        [HttpGet, Route("{id}")]
        public Invoice GetInvoice(int id)
        {
            Invoice invoice = _context.Get(id);
            if (invoice == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return invoice;
        }

        [HttpPut]
        public async Task<StatusCodeResult> PutInvoice(int[] ids)
        {
            var createInvoice = await _context.Put(ids);

            return StatusCode(createInvoice > 0 ? HttpStatusCode.OK : HttpStatusCode.NoContent);
        }

    }
}