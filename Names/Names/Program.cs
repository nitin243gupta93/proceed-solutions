﻿using System;

namespace Names
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter the file location: ");
            var file = Console.ReadLine();

            INames scoreCalculator = new CalculateScore();
            ScoreHandler scoreHandler = new ScoreHandler(scoreCalculator);
            scoreHandler.Calculate(file);

            Console.ReadLine();
        }
    }
}
