﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace Products.Models
{
    public interface IContext<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        Task<int> Put(IList<int> ids);
    }
}
