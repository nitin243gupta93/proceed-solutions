﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Products.Models;

namespace Products.Services
{
    public class InvoiceService : IContext<Invoice>
    {
        private readonly ProductsContext _productsContext;

        public InvoiceService()
        {
            _productsContext = new ProductsContext();
        }
        public IEnumerable<Invoice> GetAll()
        {
            return _productsContext.Invoices.Include(i => i.Items);
        }

        public Invoice Get(int id)
        {
            return _productsContext.Invoices.Include(i => i.Items).FirstOrDefault(w => w.Id == id);
        }

        public async Task<int> Put(IList<int> ids)
        {
            var products = ids.Select(productId => _productsContext.Products.Include(i => i.Province)
                                .FirstOrDefault(f => f.Id == productId)).Where(product => product != null).ToList();
            double? tax = 0;
            double? subtotal = 0;

            foreach (var product in products)
            {
                subtotal += product.Price;
                tax += product.Price * (product.Province.FederalTax + product.Province.ProvincialTax);
            }

            _productsContext.Invoices.AddOrUpdate(invoice => invoice.Id,
                new Invoice() { InvoiceDate = DateTime.Now, Items = products, SubTotal = subtotal, Tax = tax, Total = subtotal + tax });

            return await _productsContext.SaveChangesAsync();
        }
    }
}