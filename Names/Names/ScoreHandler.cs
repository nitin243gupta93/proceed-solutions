﻿using System;

namespace Names
{
    public class ScoreHandler
    {
        private readonly INames _score;

        public ScoreHandler(INames score)
        {
            _score = score;
        }
        public ScoreHandler()
        {
            _score = new CalculateScore();
        }

        public void Calculate(string file)
        {
            var names = _score.GetNames(file);
            var result = _score.CalculateTotal(names);

            Console.WriteLine(result);
        }
    }
}
