namespace Products.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInvoice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceDate = c.DateTime(),
                        Tax = c.Double(),
                        SubTotal = c.Double(),
                        Total = c.Double(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Products", "Invoice_Id", c => c.Int());
            CreateIndex("dbo.Products", "Invoice_Id");
            AddForeignKey("dbo.Products", "Invoice_Id", "dbo.Invoices", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Invoice_Id", "dbo.Invoices");
            DropIndex("dbo.Products", new[] { "Invoice_Id" });
            DropColumn("dbo.Products", "Invoice_Id");
            DropTable("dbo.Invoices");
        }
    }
}
