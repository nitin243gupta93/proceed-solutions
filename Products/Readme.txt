1. Open the application
2. Open Package Manager Console
3. Run command - update-database
4. Run the application
5. Use Postman or other third party extensions to test following API Calls: (port might differ)
	a. http://localhost:55036/Api/Products 
	b. http://localhost:55036/Api/Products/1
	c. http://localhost:55036/Api/Products/AllProducts
	d. Make a PUT call http://localhost:55036/Api/Invoices
		Inside of body - [1,2] (change Text to application/json)
	e. http://localhost:55036/Api/Invoices
	f. http://localhost:55036/Api/Invoices/AllInvoices
	g. http://localhost:55036/Api/Invoices/1