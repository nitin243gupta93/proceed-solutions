﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Products.Models
{
    public class ProductsContext : DbContext
    {
        public ProductsContext() : base("name=ProductsContext")
        {
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Invoice> Invoices { get; set; }

        //    public IEnumerable<Product> GetAll()
        //    {
        //        foreach (var product in Products.Include(p => p.Province))
        //        {
        //            product.Price += product.Price * (product.Province.FederalTax + product.Province.ProvincialTax);
        //        }
        //        return Products;
        //    }

        //    public Product Get(int id)
        //    {
        //        foreach (var product in Products.Include(p => p.Province))
        //        {
        //            product.Price += product.Price * (product.Province.FederalTax + product.Province.ProvincialTax);
        //        }
        //        return Products.FirstOrDefault(w => w.Id == id);
        //    }
        //}
    }
}
