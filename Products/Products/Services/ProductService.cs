﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Products.Models;

namespace Products.Services
{
    public class ProductService : IContext<Product>
    {
        private readonly ProductsContext _productsContext;

        public ProductService()
        {
            _productsContext = new ProductsContext();
        }

        public IEnumerable<Product> GetAll()
        {
            return _productsContext.Products.Include(i => i.Province);
        }

        public Product Get(int id)
        {
            return _productsContext.Products.Include(i => i.Province).FirstOrDefault(w => w.Id == id);
        }

        public Task<int> Put(IList<int> ids)
        {
            return null;
        }
    }
}