using Products.Models;

namespace Products.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProductsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProductsContext context)
        {
            context.Provinces.AddOrUpdate(province => province.Id,
                new Province() { Id = 1, Name = "Alberta", FederalTax = 0.05, ProvincialTax = 0 });

            context.Products.AddOrUpdate(product => product.Id,
              new Product() { Id = 1, Name = "Hammer", Price = 6.99, ProvinceId = 1 },
              new Product() { Id = 2, Name = "Wrench", Price = 12.99, ProvinceId = 1 }
            );
        }
    }
}
