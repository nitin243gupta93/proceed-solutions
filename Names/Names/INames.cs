﻿using System.Collections.Generic;

namespace Names
{
    public interface INames
    {
        IEnumerable<string> GetNames(string path);
        int CalculateTotal(IEnumerable<string> names);
        int GetAlphabeticalSum(string name);
    }
}
